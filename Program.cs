﻿using System;
using System.Collections.Generic;

namespace exercise23
{
    class Program
    {
        public static void Main(string[] args)
        {
        
        //TASK A
        //Adding 5 movies to the dictionary
        var movies = new Dictionary<string, string>();
        
        movies.Add("Now You See Me", "Heist Thriller");
        movies.Add("Lives of Others", "Drama");
        movies.Add("The Dark Knight","Action");
        movies.Add("Kingdom of Heaven", "Drama");
        movies.Add("Hunt fot the Wilderpeople", "Comedy");
        
        foreach (var x in movies)
        {
            Console.WriteLine($"{x.Key} is a {x.Value}");

            }
       
        //TASKB
        //Print movies in Drama genre

        foreach (var x in movies)
        {
            if (x.Value == "Drama")
            {
                Console.WriteLine($"The dramas in the list are {x.Key}");
            }
        }


        Console.ReadKey();
        //TASK C
        //Count numbers of movies in genres
        }
    }
}